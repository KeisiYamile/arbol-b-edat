
class BNode {
	private int data;
	private BNode left = null, right = null, p1 = null;
	public int key;
	private boolean leaf;
	private NodoP[]Claves;
	private BNode[]Ramas;
	public BNode der = new BNode();
	public BNode izq = new BNode();
	public BNode p=new BNode();

	public BNode(int key){ 
		this.key = key; 
	} 

	public BNode(){
	} 

	public void orderNode(int i,int j){
		Ramas = new BNode[i];
		Claves = new NodoP[j];
	}

	BNode(NodoP clave){
		Claves[0] = clave;
	}

	public int compareTo(BNode x,BNode y) {

		int a=x.key;
		int b=y.key;
		
		if (a==(b)) {   
			return 0;      
		}
		else if (a>b) {    
			return 1;      
		}else{
			return -1;
		}
	}
}

class NodoP{
	int nump;

	NodoP(int val){
		nump=val;
	}
	NodoP(){
	}
}