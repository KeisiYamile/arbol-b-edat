import org.junit.Test;
import junit.framework.TestCase;

public class TestCasesBTree extends TestCase{

	/*@Test	
	public void testInserBtree(){
		BTree t = new BTree ();

		assertTrue(t.insert(50));
		assertTrue(t.insert(7));
		assertTrue(t.insert(11));
		assertTrue(t.insert(35));
		assertTrue(t.insert(50));
		assertTrue(t.insert(8));
		assertTrue(t.insert(61));
		assertTrue(t.insert(15));
	}

	@Test	
	public void testSearchNodeBtree(){
		BTree  t2 = new BTree ();

		assertTrue(t2.searchNode(7));
		assertTrue(t2.searchNode(27));
		assertTrue(t2.searchNode(44));
		assertTrue(t2.searchNode(10));
		assertTrue(t2.searchNode(21));
		assertTrue(t2.searchNode(1));

	}
	*/
	@Test	
	public void testVacioTree(){
		
		BTree t3 = new BTree ();
		assertTrue(t3.VacioTree());
		t3.order(5);
		assertFalse(t3.VacioTree());
		

	}
	
	@Test	
	public void testVacioNode(){
		
		BTree t4 = new BTree ();
		t4.order(5);
		assertTrue(t4.VacioNode(t4.root));
		t4.insert(7);
		t4.insert(9);
		assertFalse(t4.VacioNode(t4.root));

	}
	
	@Test
	public void testComparable(){
		BTree y = new BTree();
		BNode x1 = new BNode(4);
		BNode x2 = new BNode(5);
		assertEquals(-1, y.compareTo(x1, x2));
	
	}

}