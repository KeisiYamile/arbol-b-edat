class BTree extends BNode{

	BNode aux=new BNode();

	public int almacenRaizMin;
	public int almacenRaizMax;
	public int hijosRaizMin;
	public int hijosRaizMax;
	public int almacenNodoMin;
	public int almacenNodoMax;
	public int hijosNodoMin;
	public int hijosNodoMax;

	public BNode [] root=new BNode[almacenRaizMax];

	/*Nodo Raíz:
	 * 	Almacena 1 a 2n
	 * 	Hijos mínimo 2 
	 *Nodos Internos y hojas
	 *	Almacena n a 2n            
	 *	Hijos n+1 a 2n+1
	 *		hojas no tienen
	 */

	public void nodePush(){

	}
	public boolean VacioTree(){

		if (root.length==0)
			return true;

		return false;
	}

	public boolean VacioNode(BNode[] nodo){
		int cont = 0;
		for (int i = 0; i < almacenNodoMax; i++) {
			if (nodo[i]!=null){
				cont++;
				i = almacenNodoMax;
			}
		}
		if(cont==0)
			return true;

		return false;
	}

	public boolean NodeFull(BNode[] nodo){
		int cont = 0;
		for (int i = 0; i < almacenNodoMax; i++) {
			if (nodo[i]!=null){
				cont++;
			}
		}
		if(cont+1 == almacenNodoMax)
			return true;

		return false;
	}

	public boolean insert(int k) {

		BNode x = new BNode(k);

		if(!VacioNode(root)){
			for (int i = 0; i < almacenNodoMax; i++) {
				if (root[i]==null){
					root[i]=x;
					i = almacenNodoMax;
				}
			}
		}

		if (root.length == almacenRaizMax){
			partirArray(root,x);
		}
			
		else{
			for (int i = 0; i < almacenRaizMax; i++) {

				if (compareTo(root[i],x)==-1){
					for(int j=0; j < almacenNodoMax; j++){
						if(izq[j]==null){
							root[i].izq[j] = x;
							ordenarArray(root[i].izq);
							j = almacenNodoMax;
						}
					}
				}
			}
		}
		return true;
	}



	private void ordenarArray(BNode[] izq) {
		// TODO Auto-generated method stub
		
	}
	private void partirArray(BNode[] root2, BNode x) {
		// TODO Auto-generated method stub
		
	}
	public void order(int i){

		almacenRaizMin=1;
		almacenRaizMax=i;
		hijosRaizMin=1;
		hijosRaizMax=i+1;
		almacenNodoMin=(i/2);
		almacenNodoMax=(i);
		hijosNodoMin=i/2;
		hijosNodoMax=i;		

		orderNode(hijosNodoMax,almacenNodoMax);
		root=new BNode[almacenRaizMax];
	}

	public boolean deleteNode(int k) {
		// TODO Auto-generated method stub

		return false;
	}

	public boolean searchNode(int k) {
		// TODO Auto-generated method stub
		return false;
	}


}